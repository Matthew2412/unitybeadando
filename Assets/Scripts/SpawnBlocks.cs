﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBlocks : MonoBehaviour {

    public GameObject EnemyCube;
    public GameObject PointSphere;
    public float spawnSpeed;


    void Spawn()
    {
        int randX = Random.Range(0, Camera.main.pixelWidth);

        int y = Camera.main.pixelHeight;

        Vector3 Target = Camera.main.ScreenToWorldPoint(new Vector3(randX, y, 0));
        Target.z = 0;


        int randNum = Random.Range(1, 5);

        if (randNum == 1)
        {
            Instantiate(PointSphere, Target, Quaternion.identity);
        } else
        {
            Instantiate(EnemyCube, Target, Quaternion.identity);
        }

        
        
    }

	void Start () {
        InvokeRepeating("Spawn", 0f, spawnSpeed);
	}
	
	void Update () {
		
	}
}
